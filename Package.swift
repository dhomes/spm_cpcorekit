// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let major=1
let minor=0
let point=10009

let package = Package(
    name: "CPCoreKit",
    products: [
        .library(
            name: "CPCoreKit",
            targets: ["CPCoreKit"])
    ],
    dependencies: [
    ],
    targets: [
        .binaryTarget(
            name: "CPCoreKit",
            url: "https://bitbucket.org/dhomes/spm_cpcorekit/downloads/CPCoreKit.xcframework.zip",
            checksum: "f95996f9a76eaca1531f0366f9dd6a0b362803696526034f434cf386c9908bbf"
        )
    ]
)

