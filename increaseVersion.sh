OLDMAJOR=$(cat Package.swift | grep major | awk -F= '{print $2}')
OLDMINOR=$(cat Package.swift | grep minor | awk -F= '{print $2}')
OLDPOINT=$(cat Package.swift | grep point | awk -F= '{print $2}')
NEWPOINT=$((OLDPOINT+1))
echo $NEWPOINT

OLDSTRING="let point=${OLDPOINT}"
NEWSTRING="let point=${NEWPOINT}"
echo $OLDSTRING
echo $NEWSTRING

NEWFULLVERSION="${OLDMAJOR}.${OLDMINOR}.${NEWPOINT}"
echo $NEWFULLVERSION

sed -E "s|${OLDSTRING}|${NEWSTRING}|" Package.swift > Package1.swift
mv Package1.swift Package.swift
